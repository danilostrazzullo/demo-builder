const path = require('path')
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin');

const CONFIG = require('./config/path-config')

const srcJsDir = path.join(CONFIG.src, CONFIG.javascripts.src)
const srcCssDir = path.join(CONFIG.src, CONFIG.stylesheets.src)

// Plugins instances
const extractSass = new ExtractTextPlugin({
  filename: `${CONFIG.stylesheets.dest}/[name].bundle.css`
})
const createIndexHtml = new HtmlWebpackPlugin({
  template: path.join(CONFIG.src, CONFIG.html.src, 'index.html')
})
const copyStaticAssets = new CopyWebpackPlugin([
  {
    from: path.join(CONFIG.src, CONFIG.static.src),
  }
])


// Webpack config
module.exports = {
  // input (entry points, the default chunck-name is 'main')
  entry: [
    path.resolve(__dirname, `${srcJsDir}/index.js`),
    path.resolve(__dirname, `${srcCssDir}/main.scss`)
  ],

  // output
  output: {
   filename: `${CONFIG.javascripts.dest}/bundle.js`,
    path: path.resolve(CONFIG.dest)
  },

  // transformations
  module: {
    rules: [{
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env'],
            plugins: [
              ['transform-react-jsx', {
                pragma: 'h'
              }]
            ]
          }
        }
      },
      {
        test: /\.(sass|scss)$/,
        use: extractSass.extract({
          use: [
            {
              loader: "css-loader"
            },
            {
              loader: "sass-loader"
            }
          ],
          // Override the publicPath setting for this loader
          publicPath: '',
          // use style-loader in development
          fallback: "style-loader"
        })
      }
    ]
  },

  // plugins
  plugins: [
    extractSass,
    createIndexHtml,
    copyStaticAssets
  ],

  // sourcemaps
  devtool: 'source-map',

  // server
  devServer: {
    contentBase: path.join(__dirname, CONFIG.dest),
    compress: true,
    historyApiFallback: true,
    overlay: true
  }

};
