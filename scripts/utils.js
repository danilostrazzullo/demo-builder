const fs = require('fs')

// Utilities

function copyFile(src, dest) {

  let readStream = fs.createReadStream(src)

  readStream.once('error', (err) => {
    console.log(err)
  })

  readStream.once('end', () => {
    console.log(`Done copying "${src}" → "${dest}"`)
  })

  readStream.pipe(fs.createWriteStream(dest))
}

function isImage(filename) {
  return (/\.(gif|jpg|jpeg|png)$/i).test(filename)
}

module.exports = {
  copyFile,
  isImage
}
