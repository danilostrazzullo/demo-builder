const CONFIG = require('../config/path-config')
const UTILS = require('./utils')
const fs = require('fs')
const path = require('path')
const mkdirp = require('mkdirp')

// Path to 'img' folders
const srcImageDir = path.join(CONFIG.src, CONFIG.images.src)
const destImageDir = path.join(CONFIG.dest, CONFIG.images.dest)

// Path to 'js' folders
const srcJsDir = path.join(CONFIG.src, CONFIG.javascripts.src)

let data = {
   slides: []
}

if (!fs.existsSync(destImageDir)){
  mkdirp(destImageDir, function (err) {
    if (err) {
      console.error(err)
      return
    } else {
      console.log("Public 'img' directory created!")
      buildDestImages()
    }
  })
} else {
  buildDestImages()
}

function buildDestImages() {

  fs.readdir(srcImageDir, (err, files) => {

    const slideImages = files.filter(UTILS.isImage).map(file => {
      
      const filename = file.replace(/^([0-9]*\-)/, '').split(' ').join('-')
      const fileExtension = path.extname(filename);

      UTILS.copyFile( path.join(srcImageDir, file), path.join(destImageDir, filename) )
      
      return {
        name: filename.replace(/\.[^/.]+$/, ''),
        ext: fileExtension
      }
    })

    data.slides = slideImages
  
    const jsonData = JSON.stringify(data)
    const jsModule = `export default ${jsonData}`
    
    fs.writeFile(path.join(srcJsDir, 'data.js'), jsModule, 'utf8', () => console.log('JSON data created'))
  })

}
