// Action Types
export const SET_PAGE = 'SET_PAGE'
export const NEXT_PAGE = 'NEXT_PAGE'
export const PREV_PAGE = 'PREV_PAGE'

// Action Creators
export function changePage(actionType = NEXT_PAGE) {
  return {
    type: actionType
  }
}

export function setPage(pageIndex = 0) {
  return {
    type: SET_PAGE,
    currentIndex: pageIndex
  }
}
