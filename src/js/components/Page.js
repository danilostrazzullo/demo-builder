import { h, Component } from 'preact'
import { route } from 'preact-router';
import { bindActionCreators } from 'redux'
import { connect } from 'preact-redux'
import { setPage } from '../actions'
import data from '../data'
import IntroTip from './IntroTip'

const slideNames = data.slides.map(obj => obj.name)

console.log(slideNames);

import Slide from './Slide'
import Navigation from './Navigation'

class Page extends Component {

  componentWillMount() {
    const { query } = this.props

    if ( query ) {
      this.updateCurrentPage(query)
    }
  }
  
  componentDidMount() {

    window.addEventListener('popstate', e => {
      console.log("popstate", this.props.query);
      this.updateCurrentPage(this.props.query)
    })

  }

  componentWillReceiveProps(nextProps) {
    // console.log('--- WRP ---')
    // console.log(this.props, nextProps);
    // console.log('--- --- ---')

    const { url, query = null, currentIndex } = nextProps
        
    let regex = query ? new RegExp(`(${query})$`) : null
    let path = regex ? url.replace(regex, '') : url;

    // TODO Handle this logic in a new internal method
  
    if (this.props.currentIndex !== currentIndex){
      
      if (this.props.url !== "/" && this.props.url === url) {
        return
      }
              
    } else {

      this.updateCurrentPage(nextProps.query)
      
    }
    
  }

  shouldComponentUpdate({ currentIndex }) {
    
    if ( currentIndex === this.props.currentIndex ) {
      return false
    }

  }

  componentWillUpdate(nextProps) {
    // console.log('compWillUpdate:', this.props, nextProps);

    const { url, query = null, currentIndex } = nextProps
    let regex = query ? new RegExp(`(${query})$`) : null
    let path = regex ? url.replace(regex, '') : url;

    route(path + data.slides[currentIndex].name, true)
  }

  componentDidUpdate() {
    window.scrollTo(0, 0)
  }

  updateCurrentPage(queryString) {
    const isValidSlideName = queryString !== "undefined" ? slideNames.includes(queryString) : false
    const slideIndex = isValidSlideName ? slideNames.indexOf(queryString) : 0
    
    // console.log(this.props, queryString, isValidSlideName, slideIndex);

    this.props.setPage(slideIndex)
    
    // if the query doesn't match any slide name
    // fallback to the first one
    if ( slideNames.includes(queryString) === false && this.props.url !== "/" ) {
      window.history.pushState({}, '', './')
    }
  }

  render({ currentIndex }) {
    return (
      <section class="page-content">
        <Slide name={data.slides[currentIndex].name} ext={data.slides[currentIndex].ext} />
        <Navigation />
        
        { // Show InfoTip component if not previously discarded
          null === window.localStorage.getItem('bl_demo_intro') &&
          <IntroTip />
        }
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentIndex: state.currentIndex
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setPage: bindActionCreators(setPage, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Page)
