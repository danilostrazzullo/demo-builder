import { h, Component } from 'preact'
import { bindActionCreators } from 'redux'
import { connect } from 'preact-redux'
import { NEXT_PAGE, PREV_PAGE, changePage } from '../actions'

class NavButton extends Component {

  render({ type, label, changePage }) {
    let sign = type === NEXT_PAGE ? '-' : ''
    
    return (
      <button class="nav-button" onClick={ () => changePage(type) } title={label}>
        <svg xmlns="http://www.w3.org/2000/svg" width="64" height="112" viewBox="0 0 64 112" version="1">
          <g style="fill-rule:evenodd;fill:none;stroke-linecap:square">
            <g transform={`translate(32 56) rotate(${sign}270)`} style="stroke-width:10;stroke:#2F3435">
              <path d="M-47 23.5L0-23.5"/>
              <path d="M0-23.5L47 23.5"/>
            </g>
          </g>
        </svg>
      </button>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    changePage: bindActionCreators(changePage, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(NavButton)
