import { h } from 'preact';

export default function Error({ type, url }) {
  return (
    <div class="error">
      <h1>Joseph, abbiamo un problema!</h1>
      <p>
        <pre>{url}</pre>
        All'indirizzo richiesto non è presente una slide
      </p>
      <p>
        <a href="/">Torna all'inizio della presentazione</a>
      </p>
    </div>
  );
}
