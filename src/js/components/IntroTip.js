import { h, Component } from 'preact';

class IntroTip extends Component {

  componentDidMount() {
    const discardButtons = document.querySelectorAll('[data-discard-intro]')

    discardButtons.forEach(btn => {
      btn.addEventListener('click', () => this.handleDiscard() )
    })
  }

  handleDiscard() {
    const introTip = document.querySelectorAll('.intro-tip')[0]
    
    window.localStorage.setItem('bl_demo_intro', 0)
    introTip.classList.add('is-hidden')
  }

  render() {
    return (
      <div class="intro-tip" data-discard-intro>
        <img src="assets/arrow-keys.svg" alt="Arrow keys"/>
        <p>You can browse the presentation using the arrow keys or clicking on the buttons shown by moving the mouse cursor on the sides of the page.</p>
        <button class="intro-tip__discard">Ok, thanks!</button>
      </div>
    )
  }
}

export default IntroTip
