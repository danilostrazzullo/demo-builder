import { h, Component } from 'preact'
import { bindActionCreators } from 'redux'
import { connect } from 'preact-redux'
import NavButton from './NavButton'
import { NEXT_PAGE, PREV_PAGE, changePage } from '../actions'

class Navigation extends Component  {

  componentDidMount() {
    document.addEventListener('keyup', e => this.handleArrowKeypress(e))
  }

  handleArrowKeypress(event) {
    const sideArrowKeys = [37, 39]

    if ( sideArrowKeys.includes(event.keyCode) === false ) {
      return
    }

    let actionToPerform;

    switch (event.keyCode) {
      case 37:
        actionToPerform = PREV_PAGE
        break
      
      default:
        actionToPerform = NEXT_PAGE
        break
    }

    this.props.changePage(actionToPerform)
  }

  render({ changePage }) {
    return (
      <nav class="navigation">
        <NavButton type={PREV_PAGE} label="Go to previous page" />
        <NavButton type={NEXT_PAGE} label="Go to next page" /> 
      </nav>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    changePage: bindActionCreators(changePage, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(Navigation)
