import { h } from 'preact';

export default function Slide({ name, ext }) {
  return (
    <div class="slide-block">
      <img src={`img/${name}${ext}`} alt={name} />
    </div>
  )
}
