import { h } from 'preact'
import Router from 'preact-router'
import Page from './Page'
import Error from './Error'

export default function App() {
  return (
    <Router>
      <Page path="/" />
      <Page path=":query?" />
      <Page path="/layout/:project?/:query?" />
      <Error type="404" default />
    </Router>
  )
}
