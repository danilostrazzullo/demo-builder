import { h, render, Component } from 'preact'
import { Provider } from 'preact-redux'
import { createStore } from 'redux'
import reducer from './reducers'
import App from './components/App'
import data from './data'

const initialState = {
  currentIndex: 0,
  slides: data.slides.length
}

const store = createStore(
  reducer,
  initialState,
  // connects to Redux DevTools Browser Extension
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.querySelector('#app')
)
