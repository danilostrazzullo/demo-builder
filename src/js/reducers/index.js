import { SET_PAGE, NEXT_PAGE, PREV_PAGE } from '../actions'

export default function(state, action) {
  switch (action.type) {
    case SET_PAGE:
      return Object.assign({}, state, {
        currentIndex: action.currentIndex || state.currentIndex
      })

    case NEXT_PAGE:
      return Object.assign({}, state, {
        currentIndex: state.currentIndex === (state.slides - 1) ? 0 : state.currentIndex + 1
      })

    case PREV_PAGE:
      return Object.assign({}, state, {
        currentIndex: state.currentIndex === 0 ? (state.slides - 1) : state.currentIndex - 1
      })
  
    default:
      return state;
  }
}
