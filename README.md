# Demo Builder [_WIP_]
**DISCLAIMER:**
This tool is currently under development and is **NOT actively maintained**.

The goal of this project is to build a _production-ready_ presentation of several image slides.


## Designer Workflow
---

**1.** Export your Sketch artboards in _png_ or _jpg_, following this naming convention **01-ARTBOARDNAME** (avoid punctuation marks, eg. '**. , : ; /**' and special characters)
```
😇 GOOD
01-homepage
18-what we do
001-About_Page
02-landing-country

👿 BAD
01-homepage.final
18-what we do / projectname
001-About:page
[02]-landing-country
```

**2.** Share the exported slides with a developer 🤓

**3.** Chill! 😎🍹


## Developer Workflow
---
[Node.js](https://nodejs.org/) (v6+) and [Yarn](https://yarnpkg.com/en/docs/install) are required.

**1.** Clone locally this repository

**2.** Install dependecies
```bash
yarn
```

**3.** Copy the exported images in
```
src/img/
```

**4.** Run the **start** task. The required data will be generated.
```bash
yarn run start
```

**5.** Run the **build** task. The _production-ready_ files will be generated in the '_public_' directory
```bash
yarn run build
```

**6.** Preview the app locally
```bash
yarn run serve
```

**7.** Publish it! (_Currently this step is NOT automated_)
  - Copy the _production-ready_ files on the NAS server at
```
siti/layout/[PROJECT_NAME]/
```


## Roadmap
---

### Known Bugs
- [x] If no slide-name matches the query param redirect to first slide
- [ ] Fix routing to 404 component

### ToDos
- [x] Create task to copy '.htaccess' to '/public' directory
- [x] Scroll to top on slide change
- [x] Add arrow keys navigation
- [ ] Test the workflow
- [ ] Enable navigation via browser history buttons
- [ ] Set dynamic path to resources in index.html
- [ ] Lint js

### Enhancement
- [ ] Add fade transition on slide change
- [x] 'How to navigate' tooltip component
- [ ] Handle image build task via webpack. [Maybe using this loader](https://github.com/tcoopman/image-webpack-loader)
- Design improvements
- Automation for designers
